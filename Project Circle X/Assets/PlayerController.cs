﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    /*Variabili*/
    public float speed = 100f;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKey(KeyCode.D))
        {
            transform.Rotate(new Vector3(0, 0, transform.rotation.z + speed * Time.deltaTime));
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.Rotate(new Vector3(0, 0, transform.rotation.z - speed * Time.deltaTime));
        }
    }
}
